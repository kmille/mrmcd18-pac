# CTF Challenge

# Setup
sudo docker-compose up  
There will be an API on 127.0.0.1:5000  
You will get the [source code!](app.py)  
Try to get /flag.txt  

There is also a [solution](SOLUTION.md).  
